# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'courier_handlers/text_message_handlers/version'

Gem::Specification.new do |spec|
  spec.name          = 'courier_handlers-text_message_handler'
  spec.version       = CourierHandlers::TextMessageHandlers::VERSION
  spec.authors       = 'Zyrthofar'
  spec.email         = 'zyrthofar@protonmail.com'

  spec.summary       = 'Base text message handler'
  spec.description   = 'Base handler to deliver messages by text message.'
  spec.homepage      = 'https://gitlab.com/courier_bot/courier_handlers/text_message_handler'
  spec.license       = 'MIT'

  spec.files = %w[
    lib/courier_handlers/text_message_handler.rb
    lib/courier_handlers/text_message_handlers/version.rb
  ]

  spec.require_paths = ['lib']

  spec.add_dependency 'courier_handlers'

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
