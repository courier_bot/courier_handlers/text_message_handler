# frozen_string_literal: true

require 'courier_handlers/text_message_handler'

RSpec.describe CourierHandlers::TextMessageHandler do
  let(:instance) { CourierHandlers::TextMessageHandler.new }

  describe '.deliver' do
    subject { instance.deliver(delivery_fields) }

    let(:delivery_fields) do
      {
        'from' => '+15815551000',
        'to' => '+15815551001',
        'content' => 'Text message content',
        'extra' => 'value'
      }
    end

    let(:normalized_delivery_fields) do
      {
        'from' => '+15815551000',
        'to' => '+15815551001',
        'content' => 'Text message content'
      }
    end

    it 'raises a `NotImplementedError`' do
      expect { subject }.to raise_error(NotImplementedError)
    end

    it 'normalizes the delivery fields' do
      expect(instance).to receive(:_deliver).with(normalized_delivery_fields)

      subject
    end
  end

  describe '.callback' do
    subject { CourierHandlers::TextMessageHandler.callback({}) }

    it 'raises a `NotImplementedError`' do
      expect { subject }.to raise_error(NotImplementedError)
    end
  end
end
