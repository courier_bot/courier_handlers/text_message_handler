# frozen_string_literal: true

require 'courier_handlers'

module CourierHandlers
  # Base handler for text message handlers.
  class TextMessageHandler < BaseHandler
    # Handles the third-party service's callback and updates its delivery record.
    #
    # @params args [Hash] Third-party service's callback body.
    # @return (see super)
    def self.callback(_args)
      raise NotImplementedError
    end

    protected

    # Normalizes, validates, and cleans all of the specified fields.
    #
    # A text message handler should at least accept the following fields:
    # * `from`: Phone number from which the text message will originate.
    # * `to`: Phone number to which the text message will be delivered.
    # * `content`: Content of the text message.
    def normalize_delivery_fields(fields)
      allowed_keys = %w[from to content]
      fields.select { |key, _| allowed_keys.include?(key) }
    end
  end
end
