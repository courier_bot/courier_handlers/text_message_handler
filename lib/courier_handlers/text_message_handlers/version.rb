# frozen_string_literal: true

module CourierHandlers
  module TextMessageHandlers
    VERSION = '0.4.0'
  end
end
